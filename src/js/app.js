// const testModules = require('./test-module');
// require('../css/app.css');

/** ******** Your code here! *********** */
const courses = [
  "Mathematics",
  "Physics",
  "English",
  "Computer Science",
  "Dancing",
  "Chess",
  "Biology",
  "Chemistry",
  "Law",
  "Art",
  "Medicine",
  "Statistics",
];

function getRandomCourse() {
  const randomIndex = Math.floor(Math.random() * courses.length);
  return courses[randomIndex];
}

// Task 1
function formatUsers(userMock, additionalUser) {
  const formattedUsers = _.map(userMock, (user) => {
    return {
      gender: user.gender,
      title: _.get(user, 'name.title', ''),
      full_name: _.join([user.name.first, user.name.last], ' '),
      city: _.get(user, 'location.city', ''),
      state: _.get(user, 'location.state', ''),
      country: _.get(user, 'location.country', ''),
      postcode: _.get(user, 'location.postcode', ''),
      coordinates: {
        latitude: _.get(user, 'location.coordinates.latitude', ''),
        longitude: _.get(user, 'location.coordinates.longitude', ''),
      },
      timezone: {
        offset: _.get(user, 'location.timezone.offset', ''),
        description: _.get(user, 'location.timezone.description', ''),
      },
      email: user.email,
      b_date: user.dob.date,
      age: user.dob.age,
      phone: user.phone,
      picture: user.picture.large,
      picture_thumbnail: user.picture.thumbnail,
      id: _.join([user.id.name, user.id.value], ''),
      favorite: Math.random() < 0.5,
      course: getRandomCourse(),
      bg_color: '#' + Math.floor(Math.random() * 16777215).toString(16),
      note:
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Delectus aspernatur provident earum numquam error odit consequuntur voluptatum cupiditate modi vel blanditiis necessitatibus placeat officiis, ipsa qui rem nobis nam doloribus',
    };
  });

  const mergedArray = formattedUsers.concat(additionalUser);
  // Return only items with unique id
  return _.uniqBy(mergedArray, 'email');
}

// Task 2
function isValid(user) {
  function isValidStr(str) {
    return (
      typeof str == "string" &&
      str.length > 0 &&
      str.charAt(0).toLocaleUpperCase() == str.charAt(0)
    );
  }

  if (!isValidStr(user.full_name)) {
    console.log("full_name is not valid");
    return false;
  }
  if (typeof user.gender != "string") {
    console.log("gender is not valid");
    return false;
  }
  if (!isValidStr(user.note)) {
    console.log("note is not valid");
    return false;
  }
  if (!isValidStr(user.state)) {
    console.log("state is not valid");
    return false;
  }
  if (!isValidStr(user.city)) {
    console.log("city is not valid");
    return false;
  }
  if (!isValidStr(user.country)) {
    console.log("country is not valid "+user.country);
    return false;
  }
  if (typeof user.age !== "number" || user.age < 0 || user.age > 120) {
    console.log("age is not valid");
    return false;
  }
  if (
    typeof user.phone !== "string" ||
    (user.country === "Germany" &&!/^(0049|\+49)([0-9\s/-]+)$/.test(user.phone)) ||
    (user.country === "Ukraine" &&!/^\+380([0-9]{9})$/.test(user.phone)) ||
    (user.country !== "Germany" && user.country !== "Ukraine" && !/^[\d()+-]+$/.test(user.phone))
  ) {
    console.log("phone is not valid");
    return false;
  }
  if (typeof user.email !== "string" || !/^\S+@\S+\.\S+$/.test(user.email)) {
    console.log("email is not valid");
    return false;
  }

  return true;
}

// Task 3
function filterUsersByParams(users, filters) {
  return _.filter(users, (user) => {
    return _.every(filters, (filterValue, key) => {
      if (filterValue === undefined) return true;

      if (key === 'age') {
        if (filterValue.includes('+')) {
          const minAge = Number(filterValue.replace('+', ''));
          return user.age >= minAge;
        } else if (filterValue.includes('-')) {
          const [minAge, maxAge] = filterValue.split('-').map(Number);
          return user.age >= minAge && user.age <= maxAge;
        }
      } else if (key === 'hasPicture') {
        return filterValue ? user.picture !== '' : true;
      } else if (key === 'region') {
        switch (filterValue) {
          case 'Europe':
            return _.includes(europeanCountries, user.country);
          case 'North America':
            return _.includes(northAmericanCountries, user.country);
          case 'South America':
            return _.includes(southAmericanCountries, user.country);
          case 'Asia':
            return _.includes(asianCountries, user.country);
          case 'Africa':
            return _.includes(africanCountries, user.country);
          case 'Australia':
            return _.includes(australianCountries, user.country);
          default:
            return true;
        }
      } else if (!user.hasOwnProperty(key)) {
        return false;
      } else {
        return user[key] === filterValue;
      }
    });
  });
}

// Task 4
function sortUsersByParams(users, sortBy, sortOrder) {
  const sortedUsers = [...users];
  return _.orderBy(sortedUsers, [sortBy], [sortOrder]);
}

// Task 5
function findUserByParam(users, param, value) {
  return _.find(users, (user) => user[param] === value && user.hasOwnProperty(param));
}

// Task 6
function calculatePercentageMatching(users, filters) {
  const matchingObjects = filterUsersByParams(users, filters);
  const percentage = (matchingObjects.length / users.length) * 100;
  return percentage;
}

// Helper function to get user initials
function getInitials(name) {
  const nameParts = name.split(" ");
  if (nameParts.length >= 2) {
    return `${nameParts[0][0]}.${nameParts[1][0]}`;
  } else {
    return "";
  }
}

function getAge(dateString) {
  var today = new Date();
  var birthDate = new Date(dateString);
  var age = today.getFullYear() - birthDate.getFullYear();
  var m = today.getMonth() - birthDate.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
  }
  return age;
}

// Task 1 test
// const formattedUsers = formatUsers(randomUserMock, additionalUsers);
// console.log(formattedUsers);
// console.log(formattedUsers.length);

// Task 2 test
// const formattedUsers = formatUsers(randomUserMock, additionalUsers);
// formattedUsers.forEach(user => {
//    if (isValid(user)) console.log(user.full_name + " is valid");
//    else console.log(user.full_name + " is not valid");
// });

// Task 3 test
// const formattedUsers = formatUsers(randomUserMock, additionalUsers);

// const filters = {
//   country: "Germany",
//   age: "20-36",
// };

// const filteredUsers = filterUsersByParams(formattedUsers, filters);
// console.log(filteredUsers);

// Task 4 test
// const formattedUsers = formatUsers(randomUserMock, additionalUsers);
// const sortedUsers = sortUsersByParams(formattedUsers, "age", "desc");
// console.log(sortedUsers);

// Task 5 test
// const formattedUsers = formatUsers(randomUserMock, additionalUsers);
// const foundUser = findUserByParam(formattedUsers, 'age', 75);
// console.log(foundUser);

// Task 6 test
// const formattedUsers = formatUsers(randomUserMock, additionalUsers);
// const filters = {
//   age: "20-36",
// }
// const percentage = calculatePercentageMatching(formattedUsers, filters);
// console.log(percentage);
