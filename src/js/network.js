function httpGet(url) {
  return new Promise((resolve, reject) => {
    let xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);
    xhr.onload = function () {
      if (this.status == 200) {
        resolve(this.response);
      } else {
        let error = new Error(this.statusText);
        error.code = this.status;
        reject(error);
      }
    };

    xhr.onerror = function () {
      reject(new Error("Network Error"));
    };

    xhr.send();
  });
}

// npm run start-server
function postDataToServer(data) {
  fetch("http://localhost:3001/teachers", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  })
    .then((response) => response.json())
    .then((data) => {
      console.log("Data sent succecfully", data);
    })
    .catch((error) => {
      console.error("Error while sending data", error);
    });
}

async function getUsers(amount = 50, page = 1, seed = "32jkdfg") {
  const response = await httpGet(
    `https://randomuser.me/api/?results=${amount}&page=${page}&seed=${seed}`
  );
  const results = JSON.parse(response).results;
  return results;
}
