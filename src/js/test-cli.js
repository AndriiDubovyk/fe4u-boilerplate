import readline from "readline";
import { randomUserMock, additionalUsers } from "./mock.js";
import {
  formatUsers,
  isValid,
  filterUsersByParams,
  sortUsersByParams,
  findUserByParam,
  calculatePercentageMatching,
} from "./app.js";

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

function chooseTask() {
  rl.question("Choose a task (1-6): ", (taskNumber) => {
    taskNumber = parseInt(taskNumber);

    if (isNaN(taskNumber) || taskNumber < 1 || taskNumber > 6) {
      console.error(
        "Invalid task number. Please specify a task number between 1 and 6."
      );
      chooseTask();
      return;
    }

    const formattedUsers = formatUsers(randomUserMock, additionalUsers);

    switch (taskNumber) {
      case 1:
        console.log(formattedUsers);
        break;
      case 2:
        formattedUsers.forEach((user) => {
          if (isValid(user)) console.log(user.full_name + " is valid");
          else console.log(user.full_name + " is not valid");
        });
        break;
      case 3:
        rl.question("Enter the country: ", (country) => {
          rl.question("Enter age range (e.g., 20-36): ", (ageRange) => {
            const filters = {
              country,
              age: ageRange,
            };
            const filteredUsers = filterUsersByParams(formattedUsers, filters);
            console.log(filteredUsers);
            rl.close();
          });
        });
        break;
      case 4:
        rl.question(
          "Select a field to sort by (full_name, age, b_day, country): ",
          (sortBy) => {
            rl.question("Select sorting order (asc, desc): ", (sortOrder) => {
              const sortedUsers = sortUsersByParams(
                formattedUsers,
                sortBy,
                sortOrder
              );
              console.log(sortedUsers);
              rl.close();
            });
          }
        );
        break;
      case 5:
        rl.question(
          "Enter the parameter to search by (e.g., age): ",
          (param) => {
            rl.question("Enter the value to search for: ", (value) => {
              const foundUser = findUserByParam(formattedUsers, param, value);
              console.log(foundUser);
              rl.close();
            });
          }
        );
        break;
      case 6:
        rl.question(
          "Enter age range for percentage calculation (e.g., 20-36): ",
          (ageRange) => {
            const percentageFilters = {
              age: ageRange,
            };
            const percentage = calculatePercentageMatching(
              formattedUsers,
              percentageFilters
            );
            console.log(percentage);
            rl.close();
          }
        );
        break;
      default:
        console.error(
          "Invalid task number. Please specify a task number between 1 and 6."
        );
        chooseTask();
        break;
    }
    chooseTask();
  });
}

chooseTask();
